package com.hcltech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="Trainings")
public class Training {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int TrainingId;
	
	@Column(unique=true)
	private String TrainingTitle;
	
	private String Startdate;
	
	private String EndDate;
	
	private String Status;
	

}
