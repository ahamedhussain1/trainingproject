package com.hcltech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="Users")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int UserId;
	
	@Column(unique=true)
	private String UserName;
	
	private String Email;
	
	private String Password;
	
	private String DOJ;
	
	private int TotalExp;
	
	private String HclLaptop;
	
	private String CurrentLocation;
	
	private int NonItExp;
	
	private String PersonalLaptop;
	
	private String WorkLocation;
	
	
	
	
	

}
